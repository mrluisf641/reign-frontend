FROM node:10
WORKDIR /app
COPY ./package.json ./
RUN npm install --quiet
COPY . .
EXPOSE 3000
RUN npm run build
CMD ["npm", "start"]