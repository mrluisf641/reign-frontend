# Getting Started with Create React App

This project was bootstrapped with ReactJS

## Installation

```bash
# This will generate the necessary build for the application
$ npm install

```

Remember to assign the respective environment variables, mainly the path to consume the api

```bash
$ cp .env.example .env

```

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Installation with Docker

Now, to use it with docker they need to have this package installed, and docker compose. They perform the following commands:

```bash
# This will generate the necessary build for the application
$ docker-compose build

# And finally the application will start
$ docker-compose up

```
