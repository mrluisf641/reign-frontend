import "../assets/css/app.css";
import Feed from "./Feed";

const App = (): JSX.Element => {
  const weHackerNews = "We <3 hacer news";

  return (
    <>
      <main>
        <div data-testid="main_title" className="main-title">
          <h3>HN FEED</h3>
          <h5>{weHackerNews}</h5>
        </div>
        <Feed />
      </main>
    </>
  );
};

export default App;
