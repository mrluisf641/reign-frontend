import React, { Fragment, useEffect, useState } from "react";
import feedService from "../services/feed.service";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import "../assets/css/feed.css";
import moment from "moment";
import { Hit } from "../interfaces/hit.interface";

const Feed = (): JSX.Element => {
  const [hits, setHits] = useState<Hit[]>();

  const fetchFeeds = async () => {
    await feedService.hits().then((res) => {
      const { data } = res;
      const { body } = data;

      const listSorter = body.sort((a: Hit, b: Hit) => {
        const aM = moment(a.created_at).valueOf();
        const bM = moment(b.created_at).valueOf();

        return bM - aM;
      });

      setHits(listSorter);
    });
  };

  const onDestroyHit = async (id: string): Promise<void> => {
    await feedService.destroyHit(id).then((response) => {
      console.log(response);
      if (hits) {
        const newList: Hit[] = hits.filter((item) => item._id !== id);
        setHits(newList);
      }
    });
  };

  useEffect(() => {
    fetchFeeds();
  }, []);

  return (
    <>
      <ul data-testid="list_hits">
        {hits &&
          hits.map((hit: Hit, key: number) => {
            if (hit.story_title || hit.title) {
              return (
                <Fragment key={hit.objectID}>
                  <li>
                    <a href={hit.url ? hit.url : hit.story_url}>
                      <span className="title-list">
                        {hit.title ? hit.title : hit.story_title}{" "}
                        <span>- {hit.author}-</span>
                      </span>
                      <span className="date-list">
                        {moment(hit.created_at).format(" h:mm a")}
                        <button
                          className="btn-delete-hit"
                          onClick={() => onDestroyHit(hit._id)}
                          id={"btn-" + key}
                        >
                          <FontAwesomeIcon icon={faTrashAlt} />
                        </button>
                      </span>
                    </a>
                  </li>
                </Fragment>
              );
            }
            return false;
          })}
      </ul>
    </>
  );
};

export default Feed;
