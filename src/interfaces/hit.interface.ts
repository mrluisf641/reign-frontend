export interface _highlightResultI {
  author: {
    value: string;
    matchLevel: string;
    matchedWords: [];
  };
  comment_text: {
    value: string;
    matchLevel: string;
    fullyHighlighted: boolean;
    matchedWords: string[];
  };
  story_title: {
    value: string;
    matchLevel: string;
    matchedWords: [];
  };
  story_url: {
    value: string;
    matchLevel: string;
    matchedWords: [];
  };
}

export interface Hit {
  _tags: string[];
  _id: string;
  title: string | null;
  url: string | undefined;
  author: string | null;
  points: number | null | string;
  story_text: string | null;
  comment_text: string | null;
  num_comments: string | null;
  story_id: string | null;
  story_title: string;
  story_url: string | undefined;
  parent_id: number | null;
  created_at_i: number | null;
  objectID: string;
  _highlightResult: _highlightResultI;
  created_at: Date;
}
