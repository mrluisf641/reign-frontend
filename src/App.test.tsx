import React from "react";
import { render } from "@testing-library/react";
import Feed from "./components/Feed";
import App from "./components/App";

describe("App Test", () => {
  it("render list in feeds", () => {
    const renderElement = render(<Feed />);
    expect(renderElement.queryByTestId("list_hits")).toBeInTheDocument();
  });

  it("render title in app", () => {
    const renderElement = render(<App />);
    expect(renderElement.queryByTestId("main_title")).toBeInTheDocument();
  });
});
